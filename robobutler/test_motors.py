import RPi.GPIO as io
import time

io.setmode(io.BCM)
m1a = 17 #Motor 1 Forwards
m1b = 18 #Motor 1 Backwards
m2a = 17 #Motor 2 Forwards
m2b = 18 #Motor 2 Backwards
pins = (m1a,m1b,m2a,m2b)
for i in pins:
  io.setup(i,io.OUT)

#test motors by spinning each one way then the other with 0.5 sec delay
for i in pins:
  print ('Testing pin ' + str(i))
  io.output(i, 1)
  time.sleep(0.5)
  io.output(i, 0)
