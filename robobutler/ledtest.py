#!/usr/bin/python
#based on Matt Hawkins' code http://www.raspberrypi-spy.co.uk/?p=1101
#Re written by Ryan Walmsley

import time
import RPi.GPIO as io

io.setmode(io.BCM)
leda = 24 #led in head
ledz = 25 #led pin1
pins = (leda,ledz)
for i in pins:
  io.setup(i,io.OUT)

for i in pins:
  io.output(i,True)


time.sleep(5)

for i in pins:
  io.output(i,False)

exit
