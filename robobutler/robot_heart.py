#!/usr/bin/python

import time
import atexit

import cwiid
import RPi.GPIO as io
import os
import random

io.setmode(io.BCM)
heartPin = 12
io.setup(heartPin,io.OUT)

while True:
  io.output(heartPin,False)
  time.sleep(0.5)
  io.output(heartPin,True)
  time.sleep(0.5)
