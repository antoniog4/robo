#!/usr/bin/python
#based on Matt Hawkins' code http://www.raspberrypi-spy.co.uk/?p=1101
#Re written by Ryan Walmsley
# Modified by Antonio Barrera for speech and use of Adafruit Motor Hat

from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor

import time
import atexit

import cwiid
import RPi.GPIO as io
import os
import random

io.setmode(io.BCM)

# set defaults for espeak

voiceSettings = "-ven-us+m5 -s 160  -p 70 ";

# LEDs

# green - pin 24
# red - pin 12 - heart
# yellow - pin 6
# blue - pin 26 - eyes

ledb = 26
ledr = 12
ledy = 6
ledg = 24
leds = 25

pins = (ledb,ledr,ledy,ledg,leds);

for i in pins:
  io.setup(i,io.OUT)

for i in pins:
  io.output(i,False)

roboPhrases = [
'I like warm hugs',
'Lets dance ',
'Does 2+2=4',
'Banana',
'The sky is blue',
'L o l',
'My bounty is as boundless as the sea, My love as deep; the more I give to thee, The more I have, for both are infinite. - Romeo and Juliet',
'Good night, good night! parting is such sweet sorrow, That I shall say good night till it be morrow - Romeo and Juliet',
'A big boom',
'Ba ba ba ba ba',
'Shut up and dance with me',

];


roboJokes = [
'Why was 6 afraid of 9?',
'What do you call security guards working outside Samsung shops',
'Why was the robot angry?',
'What is a robot\'s favorite type of music?',
'What did the spider do on the computer?',
'What does a robot call his father?',
'Who says sticks and stones may break my bones, but words will never hurt me?',
'What do you get when you cross a fish and drumsticks?',
'Why did Mickey Mouse go to outer space?',
'Why did the cow go to outer space?',
]

roboPunchlines = [
'Because 7 8 9',
'Guardians of the Galaxy',
'Because someone kept pushing his buttons!',
'Heavy metal!',
'Made a website!',
'Day Ta',
'A guy who has never been hit with a dictionary.',
'Fishsticks',
'He was looking for Pluto.',
'To visit the milky way.',
]


button_delay = 0.1;


# create a default object, no changes to I2C address or frequency
mh = Adafruit_MotorHAT(addr=0x60)

# recommended for auto-disabling motors on shutdown!
def turnOffMotors():
	mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)

atexit.register(turnOffMotors)

################################# DC motor config!

# Motor 1 - Front Right
frM = mh.getMotor(1)
frM.setSpeed(255)
frM.run(Adafruit_MotorHAT.FORWARD);
frM.run(Adafruit_MotorHAT.RELEASE);

# Motor 2 - Front Left
flM = mh.getMotor(2)
flM.setSpeed(255)
flM.run(Adafruit_MotorHAT.FORWARD);
flM.run(Adafruit_MotorHAT.RELEASE);

# Motor 3 - Rear Right
rrM = mh.getMotor(3)
rrM.setSpeed(255)
rrM.run(Adafruit_MotorHAT.FORWARD);
rrM.run(Adafruit_MotorHAT.RELEASE);

# Motor 4 - Rear Left
rlM = mh.getMotor(4)
rlM.setSpeed(255)
rlM.run(Adafruit_MotorHAT.FORWARD);
rlM.run(Adafruit_MotorHAT.RELEASE);

print 'Press 1 + 2 on your Wii Remote now ...'
io.output(leds, True);
# ADD LED For prompt to connect Wii Remote
time.sleep(3)

# Try to connect to the Wiimote & quit if not found
try:
  wii=cwiid.Wiimote();
  io.output(leds,False);
  io.output(ledb,True);
  io.output(ledy,True);
  os.system('sudo python robot_heart.py & >/dev/null');
except RuntimeError:
  print "Can't connect to Wiimote";
  turnOffMotors();
  io.output(leds,False)
  quit()

print 'Wiimote connected'
os.system('amixer set PCM,0 90% >/dev/null');
os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format("Hello, Robo is alive"))

wii.rpt_mode = cwiid.RPT_BTN

while True:

  buttons = wii.state['buttons']
  if (buttons & cwiid.BTN_UP):
    #Forwards
    time.sleep(button_delay)
    flM.run(Adafruit_MotorHAT.FORWARD);
    frM.run(Adafruit_MotorHAT.FORWARD);
    rlM.run(Adafruit_MotorHAT.FORWARD);
    rrM.run(Adafruit_MotorHAT.FORWARD);

   
  elif (buttons & cwiid.BTN_DOWN):
    #Backwards
    time.sleep(button_delay)
    flM.run(Adafruit_MotorHAT.BACKWARD);
    frM.run(Adafruit_MotorHAT.BACKWARD);
    rlM.run(Adafruit_MotorHAT.BACKWARD);
    rrM.run(Adafruit_MotorHAT.BACKWARD);

  
  elif (buttons & cwiid.BTN_LEFT):
    time.sleep(button_delay)
    flM.run(Adafruit_MotorHAT.BACKWARD);
    frM.run(Adafruit_MotorHAT.FORWARD);
    rlM.run(Adafruit_MotorHAT.BACKWARD);
    rrM.run(Adafruit_MotorHAT.FORWARD);
   
  elif(buttons & cwiid.BTN_RIGHT):
    time.sleep(button_delay)
    flM.run(Adafruit_MotorHAT.FORWARD);
    frM.run(Adafruit_MotorHAT.BACKWARD);
    rlM.run(Adafruit_MotorHAT.FORWARD);
    rrM.run(Adafruit_MotorHAT.BACKWARD);

  elif(buttons & cwiid.BTN_A):
    time.sleep(button_delay)
    #espeak.synth("I am awesome.");
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format("I am awesome"))


  elif(buttons & cwiid.BTN_B):
    time.sleep(button_delay)
    #espeak.synth("Hello, it's me.");
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format("Hello, it's me"));

  elif(buttons & cwiid.BTN_1):
    time.sleep(button_delay);
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format(random.choice(roboPhrases)));

  elif(buttons & cwiid.BTN_2):
    time.sleep(button_delay);
    choice = random.randint(1, len(roboJokes));
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format(roboJokes[choice]));
    time.sleep(3);
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format(roboPunchlines[choice]));
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format("Ha Ha Ha Ha Ha Ha"));

  elif(buttons & cwiid.BTN_PLUS):
    time.sleep(button_delay);
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format('Hi'));

  elif(buttons & cwiid.BTN_MINUS):
    time.sleep(button_delay);
    os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format('Good bye'));

    #story = 'I was moving around the house after Dad, Nia, and Mom were asleep but like always Taylor was awake. Our dog Chase was down here she is my best bud and the only one who is alive who knows my secret. Sadly my friends Cookie, Chocolate, and Frozen knew but they fell asleep in death. I went down stairs to see my mom and dad. My mom was washing some clothes and my dad drying them. I laid down with my mom and feel into a deep sleep.';
    #os.system('espeak '+voiceSettings+' "{0}" >/dev/null'.format(story));

  else:
    flM.run(Adafruit_MotorHAT.RELEASE);
    frM.run(Adafruit_MotorHAT.RELEASE);
    rlM.run(Adafruit_MotorHAT.RELEASE);
    rrM.run(Adafruit_MotorHAT.RELEASE);


